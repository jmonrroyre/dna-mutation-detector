const express = require("express");
const router = express.Router();
const mutationController = require("../controllers/mutation");
const auth = require("../controllers/auth");

router.post("/auth", auth.login);

router.use(auth.checkAuth);

router.post("/mutation", mutationController.hasMutation);

router.get("/stats", mutationController.mutationStats);

module.exports = router;