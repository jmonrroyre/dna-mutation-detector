const functions = require("../utils/functions");

const assert = require('assert');

describe('hasMutation',() => {
	
	describe('validateSize',() => {
		it('Revisa que los valores enviados, únicamente sean los permitidos', async function() {
	     async ()=> {
		    await assert.rejects( 
		      functions.validateSize(["ATGCGAA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTTT"],6),
		      {message: 'La matriz enviada, debe ser de la forma N*N'}
		    )
		  }
		});
	});

	describe('validateValues',() => {
		it('Valida si la matriz es de NxN', function() {
	     async ()=> {
		    await assert.rejects( 
		      functions.validateValues(["ATGCGAA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACT1"]),
		      {message: 'La matriz contiene valores no permitidos (Valores permitidos: A,T,C,G)'}
		    )
		  }
		});
	});

	describe('validateDNA',() => {
		it('DNA sin mutación', function() {
			return (functions.validateDNA(["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"])).then(result => {
		      assert.equal(result, false)
		    })
		});

		it('DNA con mutación horizontal', function() {
			return (functions.validateDNA(["TTGCAA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"])).then(result => {
		      assert.equal(result, true)
		    })
		});


		it('DNA con mutación vertical', async function() {
			const dnaTranspose = await functions.matrixTransposeNXN(["TTGCGA","CAGTGC","TTATGT","AGAAGG","CCACTA","TCACTG"],6);
			return (functions.validateDNA(dnaTranspose)).then(result => {
		      assert.equal(result, true)
		    })
		});

		it('DNA con mutación oblicua', async function() {
			const dnaOblique = await functions.matrixObliqueNXN(["ATGCAA","CAGTGC","TTATGT","AGAAGG","CACCTA","TCACTG"],6);
			return (functions.validateDNA(dnaOblique)).then(result => {
		      assert.equal(result, true)
		    })
		});

		it('Insertar registro', async function() {

		});

	});

});

describe('mutationStats',() => {

	describe('getStatsDNA',() => {
		
		it('Calcula el Ratio de mutaciones', function() {
			return (functions.calculatedRatioByTwo([2,9])).then(result => {
		      assert.equal(result.ratio, 0.2)
		    })
		});

	});

})