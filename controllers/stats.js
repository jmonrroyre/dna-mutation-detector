const sequelize = require("../utils/db")
const tables = require("../models/db")
const functions = require("../utils/functions");

const queryMutation = (option)=>{
	return new Promise(async function (resolve, reject) {
	  	const sinMutacion = await tables.tableDNA.count({distinct: 'mutation',where: {mutation: option}}).then(result =>{
			resolve(result);
		}).catch(err =>{
			reject(new Error(err.message));
		})
	});
}

const getStatsDNA =  (secuencia,mutation,vertical,horizontal,oblique)=>{
	return new Promise(async function (resolve, reject) {

		Promise.all([queryMutation(1), queryMutation(0)]).then(async values => {
			if (values.every(val=>val!=0)) {
			  	const res = await functions.calculatedRatioByTwo(values);
				resolve(res);
			} else {
				reject(new Error("No se cuenta con registros"));
			}
		}).catch(err=>{
			reject(new Error(err.message));
		});

	});
}

module.exports = {
	getStatsDNA:getStatsDNA,
	queryMutation:queryMutation
}