const jwt = require('jsonwebtoken');

const check = require("../controllers/select");

exports.login = async (req, res, next)=>{
	try{
		const {validatePass,id} = await check.checkUser(req.body.email,req.body.password)
		if (validatePass === true) {
			const token = jwt.sign({data:{id:id, email: req.body.email}},process.env.JWT_SECRET,{expiresIn:"1h"});
			res.status(200).json({"token":token});
			res.end();
		} else {
			res.status(401).json({
				error: true,
				msg: "Credenciales no válidas",
			});
			res.end();
		}
	} catch(err){
		res.status(403).json({
			error: true,
			msg: "Usuario no registrado",
		});
		res.end();
	}
}

exports.checkAuth = (req, res, next) =>{
	let token = req.get("Authorization");
	jwt.verify(token,process.env.JWT_SECRET,(err, decoded)=>{
		if(err){
			res.status(401).json({
				error: true,
				msg: "No autorizado - Debe generar un token",
			});	
			res.end();	
		}
		next();
	})
}