const bcrypt = require('bcryptjs');

const sequelize = require("../utils/db");
const tables = require("../models/db")

const checkDNA = (secuencia)=>{
	return new Promise(async function (resolve, reject) {
		const sinMutacion = await tables.tableDNA.count({distinct: 'secuencia',where: {secuencia: `${secuencia}`}})
		resolve(sinMutacion);
	});
}

const checkUser = (email, pass)=>{
	return new Promise(async function (resolve, reject) {
		try{
			const {id, passUsuario} = await getUser(email);
			const validatePass = bcrypt.compareSync(pass, passUsuario);
			resolve({"validatePass" : validatePass, "id": id});
		} catch(err){
			reject(new Error("Credenciales no válidas"));
		}
	});
}

const getUser = (email)=>{
	return new Promise(async function (resolve, reject) {
	  	await tables.tableUsers.findAll({attributes: ['id', 'password'],where: {email: email}}).then(res =>{
	  		if (res.length===0) {
	  			reject();
	  		}
	  		const result = JSON.parse(JSON.stringify(res)); 
			resolve({"id" : result[0].id, "passUsuario": result[0].password});
		}).catch(err => {
			reject(new Error("Credenciales no válidas"));
		})
	});
}

module.exports = {
	checkDNA:checkDNA,
	checkUser:checkUser
}