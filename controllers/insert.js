const tables = require("../models/db")

const insertDNA =  (secuencia,mutation,vertical,horizontal,oblique)=>{
	return new Promise(async function (resolve, reject) {
		await tables.tableDNA.create({
			secuencia: secuencia,
			mutation: mutation,
			vertical:vertical,
			horizontal:horizontal,
			oblique:oblique,
		}).then(res =>{
			resolve();
		}).catch(err => {
			reject(new Error(err.message));
		})
	});
}

module.exports = {
	insertDNA:insertDNA
}