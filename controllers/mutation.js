const stats = require("../controllers/stats");
const check = require("../controllers/select");
const insert = require("../controllers/insert");
const functions = require("../utils/functions");

exports.hasMutation = async (req, res, next) =>{
	try {
		
		let dna = req.body.dna.split(",").map(val=>val.toUpperCase());
		const existDNA = await check.checkDNA(dna.join(","))

		if(!existDNA){
			const sizeDNA = dna.length;
			const val1 = await functions.validateValues(dna);
			const val2 = await functions.validateSize(dna,sizeDNA);
			const resH = await functions.validateDNA(dna);
			const dnaTranspose = await functions.matrixTransposeNXN(dna,sizeDNA);
			const resV = await functions.validateDNA(dnaTranspose);
			const dnaOblique = await functions.matrixObliqueNXN(dna,sizeDNA);	
			const resO = await functions.validateDNA(dnaOblique);
			const allRes = [resH,resV,resO];

			if(allRes.every(res =>res==false)){
				await insert.insertDNA(dna.join(","),0,0,0,0);
				res.status(403).json({
					error: false,
					mutation:0,
					msg: "No se encontraron mutaciones en el ADN proporcionado",
				});
				res.end();
			} else {
				await insert.insertDNA(dna.join(","),1,resV,resH,resO);
				res.status(200).json({
					error: false,
					mutation:1,
					msg: "Se encontraron las siguientes mutaciones en el ADN proporcionado",
					types:{
						vertical: resV,
						horizontal: resH,
						oblique: resO
					}
				});
				res.end();
			}
		} else {
			res.status(400).json({
				error: true,
				msg: "El DNA ya ha sido analizado",
			});
			res.end();
		}
	} catch(err) {
		res.status(400).json({
			error: true,
			msg: err.message,
		});
		res.end();
	}
}

exports.mutationStats = async (req, res, next) =>{
	try {
		var regs = await stats.getStatsDNA()
		res.status(200).json(regs);
	} catch(err) {
		res.status(400).json({
			msg: err.message,
		});
	}
}