const Sequelize = require("sequelize");

const sequelize = require("../utils/db")

const tableDNA = sequelize.define("dna",{
	id:{
		type: Sequelize.INTEGER.UNSIGNED,
		autoIncrement: true,
		allowNull: false,
		primaryKey: true	
	},
	secuencia:Sequelize.TEXT('medium'),
	mutation:{
		type:Sequelize.BOOLEAN,
		allowNull: false,
		defaultValue: "0"
	},
	vertical:{
		type:Sequelize.BOOLEAN,
		allowNull: false,
		defaultValue: "0"
	},
	horizontal:{
		type:Sequelize.BOOLEAN,
		allowNull: false,
		defaultValue: "0"
	},
	oblique:{
		type:Sequelize.BOOLEAN,
		allowNull: false,
		defaultValue: "0"
	}
});

const tableUsers = sequelize.define("users");

module.exports = {
	tableDNA:tableDNA,
	tableUsers:tableUsers
};