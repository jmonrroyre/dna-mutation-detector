require('dotenv').config()
const helmet = require("helmet");
const express = require("express");
const bodyParser = require("body-parser");
const compression = require("compression"); 

const tables = require("./models/db")
const mutation = require("./routes/mutation");

const app = express();

app.use(helmet());
app.use(compression());
app.use(bodyParser.json());

app.use((req, res, next) =>{
	res.setHeader("Access-Control-Allow-Origin","*");
	res.setHeader("Access-Control-Allow-Methods","GET, POST");
	res.setHeader("Access-Control-Allow-Headers","Content-Type, Authorization");
	next();
});

app.use(mutation);

app.use("*", (req, res, next)=>{
	res.status(404).send("Recurso no encontrado")
	res.end();
})

tables.tableDNA.sync().then(res =>{
	app.listen(process.env.HTTP_PORT);
}).catch(err =>{
	console.log(err.message)
})