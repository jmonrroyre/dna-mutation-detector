const valoresDNA = ["A","T","C","G"];

const validateValues = (dna)=>{
	return new Promise(function (resolve, reject) {
		const valoresPermitidos = dna.join("").split("").every(val=>{
			if(!valoresDNA.includes(val)){ 
				return false
			} else {
				return true
			}
		});
		if(valoresPermitidos){ 
			resolve(true);
		} else {
			reject(new Error(`La matriz contiene valores no permitidos (Valores permitidos: ${valoresDNA.join(",")})`));
		}
	});
}

const validateSize = (dna,sizeDNA)=>{
	return new Promise(function (resolve, reject) {
		const matrizCuadrada = dna.filter(a=>(a.length != sizeDNA));
		if (matrizCuadrada.length) {
			reject(new Error("La matriz enviada, debe ser de la forma N*N"));
		}
		resolve(true);
	});
}

const validateDNA = (dna)=>{
	return new Promise(function (resolve, reject) {
		const validate = valoresDNA.some(val=>{
			return dna.some(val2=>val2.indexOf(val.repeat(4)) != -1);
		})
		resolve(validate);
	});
}

const matrixTransposeNXN = (dna,sizeDNA)=>{
	return new Promise(function (resolve, reject) {
		let newMatrix = []
		for (var i = 0; i < sizeDNA; i++) {
			if (newMatrix[i] === undefined) {newMatrix[i] = []};
			for (var j = 0; j < sizeDNA; j++) {
				newMatrix[i].push(dna[j][i])
			}
		}
		newMatrix = newMatrix.map(val=>val.join(""));
		resolve(newMatrix);
	});
}

const matrixObliqueNXN = (dna,sizeDNA)=>{
	return new Promise(function (resolve, reject) {
		let newMatrix = []
		for (var i = 0; i < sizeDNA; i++) {
			if (newMatrix[i] === undefined) {newMatrix[i] = []};
			for (var j = 0; j < sizeDNA; j++) {
				const fila = ((i+j)>=sizeDNA)?(i+j)-sizeDNA:i+j;
				newMatrix[i].push(dna[fila][j])
			}
		}
		newMatrix = newMatrix.map(val=>val.join(""));
		resolve(newMatrix);
	});
}

const calculatedRatioByTwo = (values)=>{
	return new Promise(async function (resolve, reject) {
		const si = values[0];
		const no = values[1]
		if (si == 0) {
			resolve({"count_mutations":si,"count_no_mutation":no,"ratio":0});
		} else if(no == 0){
			resolve({"count_mutations":si,"count_no_mutation":no,"ratio":1});
		} else {
			const ratio = parseInt(si)/(parseInt(si)+parseInt(no));
			resolve({"count_mutations":si,"count_no_mutation":no,"ratio":parseFloat(ratio.toPrecision(1))});
		}
	})
}

module.exports = {
	validateValues:validateValues,
	validateSize:validateSize,
	validateDNA:validateDNA,
	matrixTransposeNXN:matrixTransposeNXN,
	matrixObliqueNXN:matrixObliqueNXN,
	calculatedRatioByTwo:calculatedRatioByTwo
}