const Sequelize = require("sequelize");

const sequelize = new Sequelize(process.env.DB_SCHEMA, process.env.DB_USER, process.env.DB_PASS,
	{dialect:process.env.DB_DIALECT,host:process.env.DB_HOST,logging: false}
);

module.exports = sequelize;