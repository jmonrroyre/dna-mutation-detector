# DNA Mutation detector

Analyze a record (string) of DNA and check for a mutation.

## Usage

### Database Engine Support (sequelize)

[Click](https://github.com/sequelize/sequelize/blob/main/ENGINE.md)

### Local variables
Rename the file ".envDemo" to ".env" after, edit and set your database variables and the other ones

### Local server
```javascript
pm2 start
```

### Get JWT

Before send a GET/POST request, you should generate a *JWT* in the rout *POST /auth*

```javascript
{
  "email":"demo@demo.com",
  "password":"passdemo"
}

```

### Analyze DNA

Sending *POST* request to *"/mutation"* with a JSON like the next example

```javascript
{
  "dna":"ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCAC1C"
}
```

After the analyzes, you receive a response like that

```javascript
{
  "error": false,
  "mutation": 1,
  "msg": "Se encontraron las siguientes mutaciones en el ADN proporcionado",
  "types":{
    "vertical": true,
    "horizontal": true,
    "oblique": false
  }
}
```

### Get Stats 

Sending *GET* request to *"/stats"* without parameters for receive a JSON response like the next example

```javascript
{
  "count_mutations": 2,
  "count_no_mutation": 9,
  "ratio": 0.2
}
```

If you don't have any DNA in your database, receive the next message

```javascript
No se cuenta con registros
```
### Important

- Your string should be a matrix nxn
- Only accept the next characters in your string:  A,T,C,G
- If you make a change, updated "./test/test.js" and run:
```javascript
npm test
```
## Demo

You can test the application with email/password mentioned in *Get JWT* section make a request to http://52.27.17.182/


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)